echo "Performing Yum updates & install"
yum update -y
yum install gcc-c++ ncurses-devel zsh wget tree bind-utils tmux cmake -y
yum remove vim -y
dnf install util-linux-user -y

echo "Changing shells to Zsh for root & dataiku"
chsh -s $(which zsh) dataiku
chsh -s $(which zsh)

export DATAIKU_HOME=$(echo ~dataiku)
export GITLAB="https://gitlab.com/mdosborne/common"

for FILE in .zshrc .p10k.zsh .vimrc .vimrc_packages; do
        echo "Downloading $FILE"
        wget $GITLAB/-/raw/main/$FILE --output-document $HOME/$FILE
        su dataiku -c "wget $GITLAB/-/raw/main/$FILE --output-document $DATAIKU_HOME/$FILE"
done

echo "Install Oh My Zsh"
wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh --output-document /tmp/install.sh
chmod 777 /tmp/install.sh

install_oh_my_zsh () {
    if [ ! -d "$1/.oh-my-zsh" ] ; then
        cd /tmp && /tmp/install.sh --unattended --keep-zshrc
    else
        echo "Oh My Zsh already installed at $1"
    fi
}

install_oh_my_zsh "$HOME"
su dataiku -c "$(declare -f install_oh_my_zsh); install_oh_my_zsh $DATAIKU_HOME"
rm -rf /tmp/install.sh


wget https://gitlab.com/mdosborne/common/-/raw/main/aliases.zsh --output-document $HOME/.oh-my-zsh/custom/aliases.zsh
su dataiku -c "wget $GITLAB/-/raw/main/aliases.zsh --output-document $DATAIKU_HOME/.oh-my-zsh/custom/aliases.zsh"

echo "Downloading OMZ plugins"
download_plugins () {
    if [ ! -d "$2" ] ; then
        git clone $3 $1 $2
    else
        echo "Plugin already downloaded to $2"
    fi
}
download_plugins "https://github.com/zsh-users/zsh-autosuggestions" "$HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions"
download_plugins "https://github.com/jeffreytse/zsh-vi-mode" "$HOME/.oh-my-zsh/custom/plugins/zsh-vi-mode"
download_plugins "https://github.com/romkatv/powerlevel10k.git" "$HOME/.oh-my-zsh/custom/themes/powerlevel10k" "--depth=1"
download_plugins "https://github.com/VundleVim/Vundle.vim.git" "$HOME/.vim/bundle/Vundle.vim"

su dataiku -c "$(declare -f download_plugins); download_plugins https://github.com/zsh-users/zsh-autosuggestions $DATAIKU_HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions"
su dataiku -c "$(declare -f download_plugins); download_plugins https://github.com/jeffreytse/zsh-vi-mode $DATAIKU_HOME/.oh-my-zsh/custom/plugins/zsh-vi-mode"
su dataiku -c "$(declare -f download_plugins); download_plugins https://github.com/romkatv/powerlevel10k.git $DATAIKU_HOME/.oh-my-zsh/custom/themes/powerlevel10k --depth=1"
su dataiku -c "$(declare -f download_plugins); download_plugins https://github.com/VundleVim/Vundle.vim.git $DATAIKU_HOME/.vim/bundle/Vundle.vim"

echo "Installing Pyenv"
install_pyenv () {
    if [ ! -d "$1/.pyenv" ] ; then
        curl https://pyenv.run | bash
    else
        echo "Pyenv already installed to $1"
    fi
}
install_pyenv $HOME
su dataiku -c "$(declare -f install_pyenv); install_pyenv $DATAIKU_HOME"

echo "Compiling vim"
if [ ! -d "$HOME/vim" ] ; then
    git clone https://github.com/vim/vim.git $HOME/vim
else
    echo "Vim already cloned to $HOME/vim"
fi
cd $HOME/vim/src
./configure --with-features=huge --enable-python3interp --enable-terminal --enable-multibyte --disable-rightleft --disable-arabic --enable-fontset --with-python3-command=python3.8
make -j4
make install -j4


echo "Installing vim plugins"
vim -Es -u $HOME/.vimrc_packages +PluginInstall +qall
echo "Installed for root"
su dataiku -c "vim -Es -u $DATAIKU_HOME/.vimrc_packages +PluginInstall +qall"
echo "Installed for dataiku"
echo "Compiling YCM for root"
python3.8 $HOME/.vim/bundle/youcompleteme/install.py  --force-sudo
echo "Compiling YCM for dataiku"
su dataiku -c "python3.8 $DATAIKU_HOME/.vim/bundle/youcompleteme/install.py"
echo "symlinking"
rm -f /bin/vi
ln -s $(which vim) /bin/vi

echo "DONE"
