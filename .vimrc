set nocompatible
set shortmess=F
set noshowmode
set backspace=indent,eol,start
"set clipboard=unnamed
"set paste
set guioptions+=a
set hlsearch

" Let clangd fully control code completion
let g:ycm_clangd_uses_ycmd_caching = 0
" Use installed clangd, not YCM-bundled clangd which doesn't get updates.
let g:ycm_clangd_binary_path = exepath("clangd")

set encoding=utf-8
" set scrolloff=999

let g:indentLine_char = '|'

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'arcticicestudio/nord-vim'
Plugin 'itchyny/lightline.vim'
Plugin 'itchyny/vim-gitbranch'
Plugin 'tomtom/tcomment_vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'indentpython.vim'
"Plugin 'yggdroot/indentline'
Plugin 'valloric/youcompleteme'
Plugin 'heavenshell/vim-pydocstring'
"Plugin 'scrooloose/syntastic'
Plugin 'fisadev/vim-isort'
Plugin 'dense-analysis/ale'
Plugin 'hashivim/vim-terraform'
Plugin 'prettier/vim-prettier'
Plugin 'elzr/vim-json'
Plugin 'mtdl9/vim-log-highlighting'

call vundle#end()
filetype plugin indent on

syntax enable

set laststatus=2

let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'absolutepath', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name',
      \ },
      \ }

let g:ycm_autoclose_preview_window_after_completion = 1

function! LightlineFilename()
  let root = fnamemodify(get(b:, 'git_dir'), ':h')
  let path = expand('%:p')
  if path[:len(root)-1] ==# root
    return path[len(root)+1:]
  endif
  return expand('%')
endfunction

set number
nnoremap <S-M> :set invnumber<CR> :GitGutterSignsToggle<CR>
let g:gitgutter_enabled = 0
nnoremap <S-T> :GitGutterToggle<CR>
nnoremap <S-D> :ALEToggle<CR>

autocmd filetype c,cpp setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
autocmd FileType python setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
autocmd FileType bash,zsh,json setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
autocmd FileType tf setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
autocmd BufNewFile,BufRead *.html setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
autocmd BufNewFile,BufRead *.envrc set syntax=sh
autocmd BufNewFile,BufRead *.sh setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab


" set statusline+=%#warningmsg#
" set statusline+=%{exists('g:loaded_syntastic_plugin')?SyntasticStatuslineFlag():''}
" set statusline+=%*
"
" let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': [],'passive_filetypes': [] }
"
" let g:syntastic_python_checkers = ['pylint', 'mypy']
" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 0
" let g:syntastic_check_on_wq = 0
" let g:syntastic_python_pylint_rcfile='/Users/mosborne/.pylintrc'
" let g:syntastic_python_pylint_args = '--rcfile=/Users/mosborne/.pylintrc'
" let g:syntastic_python_mypy_args = '--config-file=/Users/mosborne/mypy.ini'
" let g:syntastic_quiet_messages = { "regex": 'import-error\|too-many-instance-attributes\|too-many-\|Too many arguments' }
" nnoremap <C-w>e :SyntasticCheck<CR>
" nnoremap <C-w>E :SyntasticToggleMode<CR>

" let g:prettier#autoformat = 1
" let g:prettier#autoformat_require_pragma = 0

set colorcolumn=101

colorscheme nord

autocmd BufWritePre *.py :%s/ \+$//ge

let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

function! XTermPasteBegin()
  set pastetoggle=<Esc>[201~
  set paste
  return ""
endfunction

let g:pydocstring_doq_path = "/Users/mike_osborne_work/Library/Caches/pypoetry/virtualenvs/dss-scanner-AYVKyP9C-py3.7/bin/doq"
let g:pydocstring_formatter = 'google'

let g:vim_isort_map = '<C-i>'
let g:ale_virtualtext_cursor = 'disabled'
let g:ale_fixers = { '*': ['remove_trailing_lines', 'trim_whitespace'],
\                    'python': ['isort', 'trim_whitespace', 'black']}
let g:ale_echo_msg_format = '%linter% - %s'
let g:ale_fix_on_save = 1
let g:ale_linters_explicit = 1
let g:ale_linters = { 'python': ['flake8', 'mypy', 'pylint']}
let g:terraform_align = 1
let g:terraform_fmt_on_save = 1

let g:vim_json_syntax_conceal = 0

nnoremap <Leader>jf :%!jq .<CR>

ab bp breakpoint()
set guifont=Menlo\ Regular:h14
