# Common aliases

# Alias aliases
alias a="alias"  # List all aliases
alias af="alias-finder"  # See if there is an alias for a command
alias afl="alias-finder --longer"  # See if there's an alias for a command containing the provided

# CL aliases
alias lc="clear && ls"
alias c="clear"

# Poetry aliases
alias pa="poetry add"
alias ps="poetry shell"

# Git aliases
alias gs="git status"
alias gps="git push"
alias gcm="git commit -m"
alias gb="git branch"
alias gc="git checkout"

# Vim aliases
alias vimp="vim +PluginInstall +qall"

# Terraform
alias tfa="terraform apply -auto-approve"
alias tfd="terraform destroy -auto-approve"
alias tfp="terraform plan"
alias tfv="terraform validate"

# Other
alias w1="watch -n 1"
alias rmf="rm -rf"

ccomp () {
    script_name=$1
    new_name=${script_name/.c/}
    cc -ansi -o $new_name $script_name
}

crun () {
    script_name=$1
    new_name=${script_name/.c/}
    cc -ansi -o $new_name $script_name
    ./$new_name
}

crund () {
    script_name=$1
    new_name=${script_name/.c/}
    cc -ansi -o $new_name $script_name
    ./$new_name
    rm -rf $new_name
}
